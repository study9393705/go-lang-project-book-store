module example.com

go 1.21.6

require (
	example.com/controllers v0.0.0-00010101000000-000000000000
	example.com/driver v0.0.0-00010101000000-000000000000
	example.com/models v0.0.0-00010101000000-000000000000
	github.com/gorilla/mux v1.8.1
	github.com/subosito/gotenv v1.6.0
)

require (
	example.com/repository/book v0.0.0-00010101000000-000000000000 // indirect
	github.com/lib/pq v1.10.9 // indirect
	golang.org/x/text v0.12.0 // indirect
)

replace example.com/models => ./models

replace example.com/driver => ./driver

replace example.com/controllers => ./controllers

replace example.com/repository/book => ./repository/book
